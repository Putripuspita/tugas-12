<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    // public function index()
    // {
    //     return view('home');
    // }
    public function register()
    {
        return view('page.register');
    }
    public function welcome(Request $request)
    {
        // dd($request->all());

        $depan = $request['fname'];
        $belakang = $request['lname'];


        return view('page.welcome', compact('depan', 'belakang'));
    }
}

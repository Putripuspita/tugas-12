<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cast extends Model
{
    protected $table = 'film';
    protected $fillable = ['nama', ' umur', 'bio'];
}

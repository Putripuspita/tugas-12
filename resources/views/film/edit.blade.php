@extends('layout.master')
@section('title')
Halaman Edit 
@endsection
@section('content')
<form method="POST" action="/film/{{$film->id}}" enctype="multipart/form-data">
  @csrf
  @method('put')
  <div class="form-group">
    <label>Judul</label>
    <input type="text" class="form-control" value="{{$film->judul}}" name="judul">
  </div>
  @error('judul')
  <div class="alert alert-danger">{{ $message }}</div>
@enderror
  <div class="form-group">
    <label>Ringkasan</label>
    <input type="text" class="form-control" value="{{$film->ringkasan}}" name="ringkasan">
  </div>
  @error('ringkasan')
  <div class="alert alert-danger">{{ $message }}</div>
@enderror
  <div class="form-group">
    <label>Tahun</label>
    <input type="text" class="form-control" value="{{$film->tahun}}" name="tahun">
  </div>
  @error('tahun')
  <div class="alert alert-danger">{{ $message }}</div>
@enderror
<div class="form-group">
  <label>Genre</label>
  <select name="genre_id" class="form-control">
      <option value="">---Pilih Genre---</option>
      @foreach ($genre as $item)
      @if ($item->id === $film->genre_id)
      <option value="{{$item->id}}" selected>{{$item->nama}}</option>
      @else
         <option value="{{$item->id}}">{{$item->nama}}</option> 
      @endif
      
      @endforeach
  </select>
</div>
<div class="form-group">
  <label>Poster</label>
  <input type="file" class="form-control" name="poster">
</div> 
  <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection
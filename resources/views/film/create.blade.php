@extends('layout.master')
@section('title')
Halaman Tambah Film 
@endsection
@section('content')
<form method="POST" action="/film" enctype="multipart/form-data">
    @csrf
    <div class="form-group">
      <label>Judul Film</label>
      <input type="text" class="form-control" name="judul">
    </div>
    @error('judul')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
    <div class="form-group">
      <label>Ringkasan</label>
      <textarea name="ringkasan" class="form-control" cols="30" rows="10"></textarea>
    </div>
    @error('ringkasan')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
    <div class="form-group">
      <label>Tahun</label>
      <input type="text" class="form-control" name="tahun">
    </div>
    @error('tahun')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
    <div class="form-group">
      <label>Genre</label>
      <select name="genre_id" class="form-control">
          <option value="">---Pilih Genre---</option>
          @foreach ($genre as $item)
          <option value="{{$item->id}}">{{$item->nama}}</option>
          @endforeach
      </select>
    </div>
    <div class="form-group">
      <label>Poster</label>
      <input type="file" class="form-control" name="poster">
    </div>
    @error('poster')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
    
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>

@endsection
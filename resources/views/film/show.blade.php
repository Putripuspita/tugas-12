@extends('layout.master')
@section('title')
Halaman Detail 
@endsection
@section('content')
<h2>{{$film->judul}}</h2>
<img src="{{asset('gambar/'.$film->poster)}}" width="10%" alt="">
<p>{{$film->ringkasan}}</p>
<p>{{$film->tahun}}</p>
<p>{{$film->genre_id}}</p>
<a href="/film" class="btn btn-primary">Kembali</a>
@endsection
@extends('layout.master')
@section('title')
Halaman Film 
@endsection
@section('content')
@auth
<a class="btn btn-primary" href="/film/create" role="button">Tambah Data</a>    
@endauth

<br>
<br>
<div class="card">
    <div class="card-header">
      <h3 class="card-title">Data Film</h3>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
      <table id="example1" class="table table-bordered table-striped">
        <thead>
        <tr>
          <th>No.</th>
          <th>Judul</th>
          <th>Ringkasan</th>
          <th>Tahun</th>
          <th>Genre ID</th>
          <th>Poster</th>
          <th>Action</th>
        </tr>
        </thead>
        <tbody>
            @forelse ($film as $key => $item)
            <tr>
        <th>{{$key+1}}</th>
        <td>{{$item->judul}}</td>
        <td>{{$item->ringkasan}}</td>
        <td>{{$item->tahun}}</td>
        <td>{{$item->genre_id}}</td>
        <td><img src="{{asset('gambar/'.$item->poster)}}" width="100%" height="300px" alt=""></td>
        <td>
            <form action="/film/{{$item->id}}" method="POST">
            <a href="/film/{{$item->id}}" class="btn btn-info btn-sm"> Detail</a>
            <a href="/film/{{$item->id}}/edit" class="btn btn-warning btn-sm"> Edit</a>
            
                @csrf
                @method('delete')
                <input type="submit" class="btn btn-danger btn-sm" value="Delete">
            </form>
        </td>
      </tr>
        @empty
            <tr>
                Data Film Masih Kosong
            </tr>
        @endforelse
        </tbody>
    </table>
</div>
<!-- /.card-body -->
</div>
        @endsection


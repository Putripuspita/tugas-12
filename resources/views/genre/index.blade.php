@extends('layout.master')
@section('title')
Halaman Genre 
@endsection
@section('content')
<a class="btn btn-primary" href="/genre/create" role="button">Tambah Data</a>
<br>
<br>
<div class="card">
    <div class="card-header">
      <h3 class="card-title">Data Genre</h3>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
      <table id="example1" class="table table-bordered table-striped">
        <thead>
        <tr>
          <th>No.</th>
          <th>Nama Genre</th>
          <th>Action</th>
        </tr>
        </thead>
        <tbody>
            @forelse ($genre as $key => $item)
            <tr>
        <th>{{$key+1}}</th>
        <td>{{$item->nama}}</td>
        <td>
            <form action="/genre/{{$item->id}}" method="POST">
            <a href="/genre/{{$item->id}}" class="btn btn-info btn-sm"> Detail</a>
            <a href="/genre/{{$item->id}}/edit" class="btn btn-warning btn-sm"> Edit</a>
            
                @csrf
                @method('delete')
                <input type="submit" class="btn btn-danger btn-sm" value="Delete">
            </form>
        </td>
      </tr>
        @empty
            <tr>
                Data Genre Masih Kosong
            </tr>
        @endforelse
        </tbody>
    </table>
</div>
<!-- /.card-body -->
</div>
        @endsection
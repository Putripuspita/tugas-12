@extends('layout.master')
@section('title')
Halaman Edit 
@endsection
@section('content')
<form method="POST" action="/genre/{{$genre->id}}">
    @csrf
    @method('put')
    <div class="form-group">
      <label>Nama Genre</label>
      <input type="text" class="form-control" value="{{$genre->nama}}" name="nama">
    </div>
    @error('nama')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
@endsection
@extends('layout.master')
@section('title')
Halaman Cast 
@endsection
@section('content')
<a class="btn btn-primary" href="/cast/create" role="button">Tambah Data</a>
<br>
<br>
<div class="card">
    <div class="card-header">
      <h3 class="card-title">Data Cast</h3>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
      <table id="example1" class="table table-bordered table-striped">
        <thead>
        <tr>
          <th>No.</th>
          <th>Nama Cast</th>
          <th>Umur</th>
          <th>Bio</th>
          <th>Action</th>
        </tr>
        </thead>
        <tbody>
            @forelse ($cast as $key => $item)
            <tr>
        <th>{{$key+1}}</th>
        <td>{{$item->nama}}</td>
        <td>{{$item->umur}}</td>
        <td>{{$item->bio}}</td>
        <td>
            <form action="/cast/{{$item->id}}" method="POST">
            <a href="/cast/{{$item->id}}" class="btn btn-info btn-sm"> Detail</a>
            <a href="/cast/{{$item->id}}/edit" class="btn btn-warning btn-sm"> Edit</a>
            
                @csrf
                @method('delete')
                <input type="submit" class="btn btn-danger btn-sm" value="Delete">
            </form>
        </td>
      </tr>
        @empty
            <tr>
                Data Cast Masih Kosong
            </tr>
        @endforelse
        </tbody>
    </table>
</div>
<!-- /.card-body -->
</div>
        @endsection
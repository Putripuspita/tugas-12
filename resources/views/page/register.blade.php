@extends('layout.master')
@section('title')
Sign In 
@endsection
@section('content')
    <h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>
    <form action="/welcome" method="post">
        @csrf
        <label>First Name</label>
        <br><br>
        <input type="text" name="fname">
        <br><br>
        <label>Last Name</label>
        <br><br>
        <input type="text" name="lname">
        <br><br>
        <label>Gender</label>
        <br><br>
        <input type="radio" name="gender">Male
        <br>
        <input type="radio" name="gender">Female
        <br>
        <input type="radio" name="gender">Other
        <br><br>
        <label>Nationality</label>
        <br><br>
        <select name="nationality">
            <option value="Indonesian">Indonesian</option>
            <option value="Singaporean">Singaporean</option>
            <option value="Malaysian">Malaysian</option>
            <option value="Australian">Australian</option>
        </select>
        <br><br>
        <label>Language Spoken</label>
        <br><br>
        <input type="checkbox"> Bahasa Indonesia
        <br>
        <input type="checkbox"> English
        <br>
        <input type="checkbox"> Other
        <br><br>
        <label>Bio</label>
        <br><br>
        <textarea name="bio" cols="30" rows="10"></textarea>
        <br><br>
        <input type="submit">
    </form>
@endsection